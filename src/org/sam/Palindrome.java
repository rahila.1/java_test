package org.sam;

public class Palindrome {

	public static void main(String[] args) {
		 int a1 = 121;
	     int b1 = 125;
	     checkPalindrome(a1);
	     checkPalindrome(b1);
	 }
	 public static void checkPalindrome(int num) {
	     int rem, rev = 0;
	     for (int temp = num; temp > 0; temp /= 10) {
	         rem = temp % 10;
	         rev = rev * 10 + rem;
	     }
	     if (rev == num) {
	         System.out.println(num + " is a palindrome.");
	     } else {
	         System.out.println(num + " is not a palindrome.");
	     }
	 }
	}

